import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

const IP = environment.backendBaseUrl || '193.205.108.4';
const PORT = environment.backendPort || '3000';
const API_ENDPOINT = '/api';
const BASE_URL = 'http://' + IP + ':' + PORT + API_ENDPOINT;

const CAPTURE = BASE_URL + '/capture/';
const POKEDEX = BASE_URL + '/pokedex';


const headers = new HttpHeaders({
  // eslint-disable-next-line @typescript-eslint/naming-convention
  'Content-Type': 'application/json'
});
const options = {headers};

@Injectable({
  providedIn: 'root'
})

export class BackendService {
  constructor(
    private http: HttpClient
  ) {
  }

  submitCapture(qrCode): Observable<any> {
    const body = {
      teamName: environment.teamName,
      submissionID: environment.submissionId,
      qrCode
    };
    return this.http.post<any>(CAPTURE, body, options);
  }

  getPokemons(): Observable<any> {
    return this.http.get<any>(POKEDEX, options);
  }

  getPokemon(id: number): Observable<any> {
    console.log(POKEDEX + '?pokemonID=' + id);
    
    return this.http.get<any>(POKEDEX + '?pokemonID=' + id, options);
  }

  getTeamCapturesInfo(): Observable<any> {
    const params = new HttpParams().append('teamName', environment.teamName);
    return this.http.get<any>(CAPTURE, {headers, params});
  }

  getTeamCaptures(): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        const params = new HttpParams().append('teamName', environment.teamName);
        this.http.get<any>(CAPTURE, {headers, params})
          .subscribe((teamsCaptureInfo) => {
            resolve(teamsCaptureInfo.captures);
          });
      } catch (error) {
        reject(error);
      }
    });
  }
}
