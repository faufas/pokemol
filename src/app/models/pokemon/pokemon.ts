import {Stats} from './stats';

export interface Pokemon {
  pokemonID: number;
  name: string;
  description: string;
  type: string[];
  stats: Stats;
  avatar: string;
}
