import {Component} from '@angular/core';
import {BackendService} from '../../service/backend.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private backendService: BackendService
  ) {
  }

  async getTeamCapturesInfo(): Promise<any> {
    return this.backendService.getTeamCapturesInfo()
      .subscribe((teamCapturesInfo) => {
        console.log(teamCapturesInfo);
        return teamCapturesInfo;
      });
  }

  async getTeamCaptures(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.backendService.getTeamCaptures()
        .then((teamCaptures) => {
          console.log(teamCaptures);
          resolve(teamCaptures);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
