import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Pokemon } from '../../models/pokemon/pokemon';

import { BackendService } from "../../service/backend.service";
import { QrScannerService } from "../../providers/qr-scanner/qr-scanner.service";
import { StubPokemonService } from "../../providers/pokemon/stub-pokemon.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  search: boolean;

  private pokemons: Pokemon[] = [];

  constructor(private backendService: BackendService, private qrReader: QrScannerService, private router: Router,
    private stubService: StubPokemonService) {
    this.search = false;

    // this.pokemons = this.stubService.getPokemons();
  }

  ngOnInit() {
    this.refreshList(null);
  }


  public catchPokemon(): void {
    this.qrReader.scanQr('Scansiona il codice QR per catturare un Pokémon!');

    this.qrReader.getReadData().then(data => {
      console.log('Read data: ' + data);

      if (!data) {
        console.log('No data!');
        return;
      }

      this.backendService.submitCapture(data).subscribe((status) => {

        console.log(status);

        // TODO manage status (show a success or an error message)
      }, (err) => {
        console.log(err);

      })
    }).catch(error => {
      // TODO display alert and show the user an error message
    })
  }

  filterList(event: any) {
    const query = event.target.value;

    this.pokemons = this.stubService.getPokemons().filter(p => p.name.toLowerCase().indexOf(query.toLowerCase()) > -1);
  }

  getList() {
    return this.pokemons;
  }

  refreshList(event: any) {
    this.backendService.getPokemons().subscribe(

      (pokemons) => {
        this.pokemons = pokemons;
      }

    );
    if (event)
      event.target.complete();
  }
}
