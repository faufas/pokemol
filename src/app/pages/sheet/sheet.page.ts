import { BackendService } from './../../service/backend.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Pokemon } from '../../models/pokemon/pokemon';
import { StubPokemonService } from '../../providers/pokemon/stub-pokemon.service';

@Component({
  selector: 'app-sheet',
  templateUrl: './sheet.page.html',
  styleUrls: ['./sheet.page.scss'],
})
export class SheetPage implements OnInit {

  pokemon: Pokemon;

  constructor(private backendService: BackendService, private route: ActivatedRoute, private router: Router, private stubService: StubPokemonService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(params);
      
      this.backendService.getPokemon(+params.id).subscribe(

        (pokemon) => {
          this.pokemon = pokemon;
          if (!this.pokemon) {
            this.router.navigate(['/not-found'])
              .then(() => {
              });
          }
        }
  
      );
    });

    
  }

}
