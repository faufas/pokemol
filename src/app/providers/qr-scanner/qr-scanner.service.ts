import {Injectable} from '@angular/core';
import {BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult} from "@ionic-native/barcode-scanner/ngx";

@Injectable({
  providedIn: 'root'
})
export class QrScannerService {

  private readData: Promise<BarcodeScanResult>;

  constructor(private qrScanner: BarcodeScanner) {
    this.readData = null;
  }

  public getReadData(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      (this.readData)
        ? this.readData.then(data => {
          resolve((data.cancelled) ? null : data.text);
        }).catch(error => {
          console.log('Error reading QR: ' + error);

          reject(error);
        })
        : reject();
    })
  }

  public scanQr(message: string): void {
    const options: BarcodeScannerOptions = {
      disableSuccessBeep: true,
      formats: 'EAN_13,EAN_8,QR_CODE,PDF_417',
      orientation: 'portrait',
      prompt: (!message) ? '' : message,
      preferFrontCamera: false,
      resultDisplayDuration: 0,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false
    };

    this.readData = this.qrScanner.scan(options);
  }
}
